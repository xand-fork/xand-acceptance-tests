# Contributing to this repo
[[_TOC_]]

## Tech debt
* Add a cleanup command into the network starter (`yarn xand-up`)
* Improve test subset selection for confidential/non-confidential code - see backlog item [ADO 6499](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6499)

### Update in XNG
* Add Ability to generate only on-chain (don't need extra off-chain entities here)
