# Acceptance Tests
[[_TOC_]]

## Network Generation
A network to test against can be generated with the following command:

```bash
./network-generator-utility/build.sh -i
```

This script will clean the old network and replace it with new a `generated` network.

To change what versions of artifacts are used in generation, update `/network-generator-utility/.versions.env`.

## Pre-requisites

Install Yarn v.1 - [instructions](https://classic.yarnpkg.com/)

Install local dependencies using yarn:

```bash
yarn install
```

## Dockerfiles
The [`Dockerfile`](.docker/Dockerfile) used in smoke tests uses a base Docker container defined [here](.docker/base/Dockerfile).
The base dockerfile is *manually published* to `gcr.io/xand-dev/node-16-python-3-8:latest` and if changes must be made to it they must be manually published again.

## Running Tests

### Run tests
> Note: see instructions for generating networks required to support these tests in [Network Generation](#network-generation)

```bash
yarn xand-purge
yarn rebuild
````

#### Run network test:
```bash
yarn xand-up
yarn xand-test
```

Tear down:

```bash
yarn xand-down
```

## Environment and Network Setup

These tests are designed to run against a live environment. Consequently, they assume a running network. If running the tests locally,
you will need to start the network first.

Requirements:
- [Install Docker Compose v2](https://docs.docker.com/compose/install/#install-compose-on-linux-systems ) (included with Docker Desktop)  
  Linux install instructions:
  ```
  mkdir -p /usr/local/lib/docker/cli-plugins
  curl -L "https://github.com/docker/compose/releases/download/v2.11.2/docker-compose-linux-x86_64" \
    -o /usr/local/lib/docker/cli-plugins/docker-compose
  chmod +x /usr/local/lib/docker/cli-plugins/docker-compose
  ```

You can start a network from the `/generated` directory after generating a network with:

    docker compose up -d



#### Cleanup

Make sure you clean up generated files with `yarn xand-clean` before re-generating to avoid spurious errors


# Testing Overrides

## Running local vs. running against Docker Compose

Member API is backed by a database containing banks configuration data. Part of that data is the url to the bank.
The acceptance tests ensure that a couple of banks and accounts exist to test with as part of the test setup process.

When running local instances of member api and bank-mocks, the correct bank url is usually `localhost:8888`.

When running inside of `xand-docker-compose` the correct bank url changes to `bank-mocks:8888` since `localhost` now refers to the container rather than the host machine.

In `./config` you will now find 2 config files:

```bash
drwxr-xr-x   4 crmckenzie  staff   128 Oct  7 14:04 .
drwxr-xr-x  26 crmckenzie  staff   832 Oct  7 14:04 ..
-rw-r--r--   1 crmckenzie  staff   699 Oct  7 14:04 config.docker.yaml
-rw-r--r--   1 crmckenzie  staff  3018 Oct  7 14:04 config.yaml
```

By default, the acceptance tests will use the values in `config.yaml`. You can apply overrides to the base config
by passing a command-line argument to `yarn test`:

```bash
yarn test --test-env docker
```

The example command instructs the test suite to apply the changes in `config.docker.yaml` to the configuration object creatd by `config.yaml`. The relevant code can be found in `Configuration.ts`:

```typescript
  public static environment(): string {
    let result = "local";

    process.argv.forEach((element, index) => {
      const str = process.argv[index];
      if (str === "--test-env") {
        result = process.argv[index + 1];
      }
    });

    return result;
  }

  public static loadTestEnvironment(): Configuration {
    const environment = Configuration.environment();
    const result = Configuration.default();
    if (environment !== "local") {
      const overrideFileName = `config/config.${environment}.yaml`;
      const override = Configuration.configFromFile(overrideFileName);
      mergeDeep<Configuration>(result, override);
    }
    return result;
  }

```

Since we already have a set of commands for working with `docker compose` that use the `xand-` prefix, we've
defined a `yarn` command for testing against our `docker compose` network that takes care of this for you: `yarn xand-test`.

# Setting up for a local Demo
Bring down any network, if running:
```bash
yarn xand-down
```

Make sure to purge any old chains

```bash
yarn xand-purge
```

Bring up the network

```bash
yarn xand-up
```

As of Member API version 3.0, bank configurations are no longer provided via a config file,
but configured via the API.

The Acceptance Tests configure the banks. Run the subset of tests relevant to bank configuration
for the docker compose environment with:

```bash
yarn xand-configure-banks
```

Start up your wallets.

For `awesomeco`, point your wallet to `http://localhost:3000`.
For `coolinc`, point your wallet to `http://localhost:3001`.

Visit those URLs to play around with those Member APIs directly via the Swagger doc!

# Editing Tests

## Coordinating Acceptance Tests changes with Component Changes

Upstream components are free to publish versions of themselves at their own cadence. Their repos
may offer convenience jobs where these tests get invoked for the particular version of the component
getting published at any point in time (for example, a version based off a feature branch, noted by a specific
commit), but these convenience jobs wouldn't necessarily be blocking in the component's CI.

In GitLab CI, the triggered job is viewable at the end in the **Downstream** section.
For example, see how `thermite` triggers these tests, specifying the component versions it produced
from a particular branch:

`.gitlab-ci.yaml`
```yaml
acceptance-test:
  stage: acceptance-test
  variables:
    MEMBER_API_VERSION: $CI_COMMIT_SHA
    VALIDATOR_VERSION: $CI_COMMIT_SHA
    TRUSTEE_VERSION: $CI_COMMIT_SHA
  trigger:
    project: transparentincdevelopment/product/testing/xand-acceptance-tests
    branch: master
    strategy: depend
```

[Pipeline](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/pipelines/129684390)

![image](./images/trigger-job-example.png)

However, components will be deemed "ready" internally once this repo references the correct version in
`master`, showing that it has passed Acceptance Tests.

For now, we will manually update the version here. We would quickly move to automating this.

Publishing minor version updates of components should not break existing ATs. In these cases, there may
not be updates required to the ATs.

However, there are times when new tests will have to be added, or existing ones modified. This would be a
2-step MR process.

1. Component gets merged to the stable branch in its own repo
    - Component's integration tests pass
    - Component version is published (docker image) off of stable branch
1. This repo is updated, referencing the newly published image, along with any updates to ATs or configs.

You can use your preferred method to start a local network with your updated component, and run modified
ATs locally to verify everything works before opening the MRs and testing in CI.

Current options to start a local network are:
- `docker compose up -d` from the `xand-docker-compose` directory in this repo
- `cargo make start-net` in `thermite`

You can publish an image of your component off of your branch, and reference the commit in the `/xand-docker-compose/.env`
file to replicate exactly what would happen in CI.

<!--TODO_: Flesh out `xand-docker-compose` consuming local binaries.-->

# CI/CD
## Scheduled Runs

See [SCHEDULED_RUNS.md](./SCHEDULED_RUNS.md) for more info on scheduling runs of the Acceptance Tests
against a deployed environment and related monitoring.

# Test Dependencies
## Client Dependencies

These acceptance tests consume three typescript clients produced internally:
    - [`xand-api-ts-client`](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/tree/develop/xand-api-ts-client/)
    - [`xand-member-api-client`](https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/tree/develop/member-api/client)
    - [`xand-trust-api-client`](https://gitlab.com/TransparentIncDevelopment/product/libs/trust-api/-/tree/master/client)


### gcloud

First, install and configure the Google Cloud (GCP) SDK from [here](https://cloud.google.com/sdk/docs/).

Set Docker as GCP's credential helper;

```
gcloud auth configure-docker
```

[Troubleshooting](https://cloud.google.com/container-registry/docs/advanced-authentication)

## Customizing Test Output

By default, the test output looks as follows:

```bash
Jasmine started

  Given a Wallet

    And a set of Bank Accounts
      ✓ Then I can Get All Banks (0.005 sec)
      ✓ Then I can Get the Balance for a bank (0.007 sec)
      ✓ Then I can Get a bank By Name (0.004 sec)

    When I submit a create request
      ✓ Then the system gives me a transaction id (0.308 sec)

    And I have submitted a create request

      When I transfer funds to the reserve
        ✓ Then the bank balance is reduced by the amount of the transfer. (0.007 sec)
        * Then a related Cash Confirmation transaction is constructed by the trustee.
        ✓ Then the xand balance is increased by the amount of the Transfer (30 secs)

**************************************************
*                    Pending                     *
**************************************************

1) Given a Wallet And I have submitted a create request When I transfer funds to the reserve Then a related Cash Confirmation transaction is constructed by the trustee.
  Need to add an endpoint for the Member API to find the correlated transaction

Executed 6 of 7 specs INCOMPLETE (1 PENDING) in 36 secs.
```

When debugging system behavior it can be useful to see the details of the HTTP requests that are occurring. You can use the `HTTP_LOG` environment variable for this. There are currently 3 optional amounts of output that will be displayed in the logs. `URL`, `REQUEST` and `RESPONSE`. These flags can be joined via spaces in the environment variable. e.g.

    export HTTP_LOG="RESPONSE"
    export HTTP_LOG="URL RESPONSE"
    export HTTP_LOG="URL REQUEST RESPONSE"

```bash
Jasmine started

  Given a Wallet

    And a set of Bank Accounts
      ✓ Then I can Get the Balance for a bank (0.008 sec)
      ✓ Then I can Get a bank By Name (0.003 sec)
      ✓ Then I can Get All Banks (0.004 sec)

export HTTP_LOG="URL"
npm test

> xand-acceptance-tests@1.0.0 test /home/crmckenzie/src/thermite/tests/acceptance
> jasmine

Jasmine started
POST http://localhost:3000/api/v1/walletFromKey
POST http://localhost:3000/api/v1/walletFromKey
GET http://localhost:3000/api/v1/bank/accounts/awesomeco

  Given a Wallet

    And a set of Bank Accounts
      ✓ Then I can Get a bank By Name (0.003 sec)
GET http://localhost:3000/api/v1/bank/accounts/awesomeco/balance
      ✓ Then I can Get the Balance for a bank (0.006 sec)
GET http://localhost:3000/api/v1/bank/accounts
      ✓ Then I can Get All Banks (0.003 sec)

POST http://localhost:3000/api/v1/xand/createRequest
    When I submit a create request
      ✓ Then the system gives me a transaction id (0.369 sec)

... snipped for brevity
```

You can also configure logging to show requests and responses.

```bash

export HTTP_LOG="URL REQUEST RESPONSE"
npm test

Jasmine started
POST http://localhost:3000/api/v1/walletFromKey
REQUEST
privateKey: bottom drive obey lake curtain smoke basket hold race lonely fit walk//2Pac
RESPONSE 200 - OK
PUBLIC_KEY:  5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ
PRIVATE_KEY: bottom drive obey lake curtain smoke basket hold race lonely fit walk//2Pac
POST http://localhost:3000/api/v1/walletFromKey
REQUEST
privateKey: bottom drive obey lake curtain smoke basket hold race lonely fit walk//Biggie
RESPONSE 200 - OK
PUBLIC_KEY:  5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ
PRIVATE_KEY: bottom drive obey lake curtain smoke basket hold race lonely fit walk//Biggie
POST http://localhost:3000/api/v1/xand/createRequest
REQUEST
accountName:       awesomeco
address:           5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ
amountInMinorUnit: 100
RESPONSE 200 - OK
transactionId: 0xdcd7e975cded3b49df9df3b7283b269aabf81bb2f01ae84c9a4e30fcdb128ec9
statusUrl:     http://localhost:3000/api/v1/xand/transaction/0xdcd7e975cded3b49df9df3b7283b269aabf81bb2f01ae84c9a4e30fcdb128ec9
correlationId:         9smesKFzDkBL6Wj8ibNJ4f

  Given a Wallet

    When I submit a create request
      ✓ Then the system gives me a transaction id (0.419 sec)
```

If you are running in a console that supports it, the output will be colorized.

## API Rest Clients
API Client libraries are auto-generated by [openapi-generator](https://github.com/OpenAPITools/openapi-generator).
We consume the `xand-api-ts-client` from our artifactory.
