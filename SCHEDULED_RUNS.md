# Scheduling and Monitoring Acceptance Tests
[[_TOC_]]
## Scheduling from GitLab

This repo can be used to schedule AT runs against a deployed environment. View existing 
[pipeline schedules](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-acceptance-tests/pipeline_schedules).

Scheduled runs are useful to help verify the long-lived health of an environment, and/or verify
functionality after deploying a new version to some test environment.

[./gitlab-ci/schedulable/jobs.yaml](./gitlab-ci/schedulable/jobs.yaml) exposes the jobs run when the
pipeline is invoked via a schedule.

You can see the `test-remote` job there, accepting the following variables:
```yaml
variables:
    MEMBER_API_URL: ""
    BANK_MOCKS_URL: ""
    XAND_API_CONNECTION: ""
    VAULT_ENVIRONMENT: dev
    K8S_NAMESPACE: "default"
```  

From the [pipeline schedules](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-acceptance-tests/pipeline_schedules)
 -> **New Schedule** page, specify the correct variables for the scheduled job to target your environment of choice.
 
![image](./images/scheduled-job-def.png)

The `K8S_NAMESPACE` variable is appended to the trace name to make filtering for specific environments easier in Honeycomb.
For example, the trace for each run against the `osha` environment would be named `yarn-test-remote-osha`.
  
## Monitoring with Honeycomb

### Queries

These jobs take advantage of the [tracing](https://gitlab.com/TransparentIncDevelopment/docs/engineering-guide/-/blob/master/Observability.md#distributed-tracing) utilities
built into our `tpfs-ci` CI templates, and emit job and command level traces to our [honeycomb instance](https://ui.honeycomb.io/signup/hd/tpfs.io).

For Scheduled Jobs, we append the `K8S_NAMESPACE` to the trace name: `yarn-test-remote-<K8S_NAMESPACE>`.

In Honeycomb, you can filter for `name = yarn-test-remote-<K8S_NAMESPACE>` to see environment specific
runs.

The Honeycomb query GUI is a nice place to explore different metrics around your scheduled runs.
For example, see this query showing passes and failures of AT runs over the previous 24 hours. 

![image](./images/honeycomb-graph.png)
 
The orange spikes indicate test failures during that timespan.
You can click into individual points on the graph to see the source trace, and from there, fetch the link
to the CI pipeline itself. This is particularly useful for fetching logs while debugging failures.

### Triggers

Honeycomb allows you to configure alerts when results of queries exceed some threshold.

[TPFS Honeycomb Triggers Page](https://ui.honeycomb.io/transparent-systems/datasets/buildevents/triggers/oGogLArxyHw?view=/transparent-systems/triggers)

You can configure alerts to go to `engineering-dev-alerts@tpfs.io`, and alerts will be automatically 
forwarded to the [#engineering-dev-alerts](https://transparentsystems.slack.com/archives/C010T5W03SL) channel.

If you choose to forward alerts to a group email within TPFS, ensure the group is set to allow emails from 
external domains. This requires Owner privileges over the group, and elevated permissions within our GSuite.
