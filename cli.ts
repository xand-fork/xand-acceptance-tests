#!/usr/bin/env node

import mochaCommands = require("mocha/lib/cli/commands");
import yargs = require("yargs");
import { run, setRequiredDefaultArgs } from "./lib";

const RUN_SUBCMD_DESCRIPTION = `Run all tests with default reporters and timeouts set. \
Mocha CLI arguments are available on this subcommand. For filepath related flags, \
only "--ignore" is supported. It requires a relative glob pattern as if within the root \
dir in the xand-acceptance-tests repo. Example: "--ignore tests/local/**"`;

const _ = yargs
    .scriptName("xat")
    .command(
        // mochaCommands.run,
        "run",
        RUN_SUBCMD_DESCRIPTION,
        mochaCommands.run.builder,
        async (args) => {
            await run(args);
        },
    )
    .demandCommand()
    .strict() // Errors if unrecognized arguments are passed
    .argv;
