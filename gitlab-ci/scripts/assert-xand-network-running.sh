#!/bin/bash

# Resurrected from Chris M.'s closed MR
# (https://gitlab.com/TransparentIncDevelopment/product/apps/thermite/-/merge_requests/1015)

set -x         # print commands before executing
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

die () {
    echo >&2 "$@"
    echo "$(helptext)"
    exit 1
}

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by [humans|automation]
    The purpose of this script is to healthcheck the 3 endpoints passed

    Arguments
        MEMBER_API_URL (Required) = Member API URL
        BANK_MOCKS_URL (Required) = Bank mocks URL
        XAND_API_CONNECTION (Required) = Xand Api Connection which will be used via a grpc endpoint.
END
)
echo "$HELPTEXT"
}


[ "$#" -eq 3 ] || die "Missing arguments"
echo $1 | grep -E -q '^http(s)?:\/\/.+(:\d+)?/api/v' || die "Member API URL not in correct format: $1"
echo $2 | grep -E -q '^http(s)?:\/\/.+(:\d+)?' || die "Bank Mocks URL not in correct format: $2"
echo $3 | grep -E -q '^.+(:\d+)?' || die "Xand API Connection not in correct format: $3"

MEMBER_API_URL=$1
BANK_MOCKS_URL=$2
XAND_API_CONNECTION=$3

# This will wait until valid certs are found for a passed in url.
# Expected required arguments are just the URL itself as the first argument.
function wait_until_tls_cert_available {
    URL=$1
    HOST_NAME=$(echo $URL | sed -e "s|https://\([^/]\+\)/\?.*|\1|")
    local COUNT=0
    local VALID_CERT=0
    while [[ $VALID_CERT -eq 0 && $COUNT -lt 90 ]]
    do
        # Searches for the default Kubernete Fake Cert
        # and considers it invalid if grep finds it.
        echo | \
            openssl s_client -showcerts $HOST_NAME:443 | \
            grep "Kubernetes Ingress Controller Fake Certificate" && \
            VALID_CERT=$? || VALID_CERT=$?

        sleep 2
        COUNT=$(($COUNT+1))
    done
}

# This will wait until valid certs are found for a passed in url
# and check if a 200 response comes back from said url.
# Expected required arguments are just the URL itself as the first argument.
function wait_until_url_available {
    URL=$1
    wait_until_tls_cert_available $URL
    curl $URL --retry-connrefused --retry-delay 2 --retry 100
}

# This will wait until valid certs are found for a passed in url
# and check if a 200 response comes back from said url assuming http2 protocols.
# Expected required arguments are just the URL itself as the first argument.
function wait_until_http2_url_available {
    URL=$1
    wait_until_tls_cert_available $URL
    curl $URL --retry-connrefused --retry-delay 2 --retry 100 --output -
}

echo "Member API starting: $MEMBER_API_URL"
wait_until_url_available $MEMBER_API_URL

echo "Banks API starting: $BANK_MOCKS_URL"
wait_until_url_available $BANK_MOCKS_URL

echo "XAND API Connection starting: $XAND_API_CONNECTION"
wait_until_http2_url_available https://$XAND_API_CONNECTION
