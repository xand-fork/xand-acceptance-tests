import { BankAccount } from "xand-member-api-client";

export function sortAccountsByShortName(accounts: BankAccount[]): BankAccount[] {
    const sorted = accounts.sort((l, r) => {
        if (l.shortName > r.shortName) {
            return 1;
        }
        if (l.shortName < r.shortName) {
            return -1;
        }
        return 0;
    });
    return sorted;
}
