import crypto from "crypto";
import { parse } from "url";
import { grpc } from "xand-api-ts-client/dist/src/index";
import { XandApiClient } from "xand-api-ts-client/dist/xand-api_grpc_pb";
import {
    AddressBalanceRequest,
    AddressTransactionHistoryRequest,
    BankAccount, Blockstamp,
    CashConfirmation,
    CreateRequest,
    FetchedTransaction, GetCurrentBlockReq, GetValidatorEmissionProgressReq,
    GetValidatorEmissionRateReq,
    RedeemFulfillment,
    RedeemRequest,
    Send, TotalIssuanceRequest,
    TransactionDetailsRequest,
    TransactionHistory,
    TransactionHistoryRequest,
    TransactionStatus, TransactionUpdate,
    UnencryptedBankAccount, UserTransaction, ValidatorEmissionProgress, ValidatorEmissionRate,
} from "xand-api-ts-client/dist/xand-api_pb";

export class EnhancedXandApi extends XandApiClient {
    private authJwt: string | null;

    constructor(address: string, authJwt: string | null) {
        const parsed = parseURlForGrpcClient(address);
        const channel = parsed.endsWith(":443")
            ? grpc.credentials.createSsl()
            : grpc.credentials.createInsecure();
        super(parsed, channel);
        this.authJwt = authJwt;
    }

    public async getBlock(): Promise<Blockstamp> {
        return new Promise((resolve, reject) => {
            const req = new GetCurrentBlockReq();
            this.getCurrentBlock(req, this.buildAuthMetadata(), (err, response) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve(response);
                }
            });
        });
    }

    public async getEmissionRate(): Promise<ValidatorEmissionRate> {
        return new Promise((resolve, reject) => {
            const req = new GetValidatorEmissionRateReq();
            this.getValidatorEmissionRate(req, this.buildAuthMetadata(), (err, response) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve(response);
                }
            });
        });
    }

    public async getEmissionProgress(address: string): Promise<ValidatorEmissionProgress> {
        return new Promise((resolve, reject) => {
            const req = new GetValidatorEmissionProgressReq();
            req.setAddress(address);
            this.getValidatorEmissionProgress(req, this.buildAuthMetadata(), (err, response) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve(response);
                }
            });
        });
    }

    public async getBalance(account: string): Promise<number> {
        return new Promise((resolve, reject) => {
            const request = new AddressBalanceRequest();
            request.setAddress(account);
            this.getAddressBalance(request, this.buildAuthMetadata(), (err, response) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve(response.getBalance().getAmount());
                }
            });
        });
    }

    public async getTotalIssuedClaims(): Promise<number> {
        return new Promise((resolve, reject) => {
            const request = new TotalIssuanceRequest();
            this.getTotalIssuance(request, this.buildAuthMetadata(), (err, response) => {
                if (err) {
                    return reject(err);
                } else {
                    resolve(response.getBalance());
                }
            });
        });
    }

    public async asyncSubmitTx(
        request: UserTransaction, options?: Partial<grpc.CallOptions>,
    ): Promise<TransactionUpdate> {
        return new Promise((resolve, reject) => {
            const submitStream = super.submitTransaction(
                request,
                this.buildAuthMetadata(),
                options,
            );
            let receivedFinal = false;
            submitStream.on("data", (data: TransactionUpdate) => {
                switch (data.getStatus()?.getStatusCase()) {
                    case TransactionStatus.StatusCase.STATUS_NOT_SET:
                    case TransactionStatus.StatusCase.RECEIVED:
                    case TransactionStatus.StatusCase.ACCEPTED:
                    case TransactionStatus.StatusCase.BROADCAST:
                        break;
                    case TransactionStatus.StatusCase.INVALID:
                    case TransactionStatus.StatusCase.DROPPED:
                        submitStream.destroy();
                        receivedFinal = true;
                        reject(data);
                        break;
                    case TransactionStatus.StatusCase.FINALIZED:
                        submitStream.destroy();
                        receivedFinal = true;
                        resolve(data);
                        break;
                }
            });
            submitStream.on("end", () => {
                if (!receivedFinal) {
                    reject("didn't receive an expected final update");
                }
            });
        });
    }

    public async createRequestClaim(account: string, amount: number):
        Promise<{ correlationId: string, createRequest: string}> {

        const correlationId = this.randomCorrelationId();

        // create request

        const unencryptedBankAccount = new UnencryptedBankAccount();
        unencryptedBankAccount.setAccountNumber("account");
        unencryptedBankAccount.setRoutingNumber("routing");

        const bankAccount = new BankAccount();
        bankAccount.setUnencryptedBankAccount(unencryptedBankAccount);

        const createReq = new CreateRequest();
        createReq.setAmount(amount);
        createReq.setCorrelationId(correlationId);
        createReq.setBankAccount(bankAccount);

        const createReqTransaction = new UserTransaction();
        createReqTransaction.setCreateRequest(createReq);
        createReqTransaction.setIssuer(account);

        const createReqReceipt = await this.asyncSubmitTx(createReqTransaction);
        if (createReqReceipt.getStatus()?.getStatusCase()
            !== TransactionStatus.StatusCase.FINALIZED) {
            throw new Error(`Create request correlationId=${correlationId} failed`);
        }

        return { correlationId, createRequest: createReqReceipt.getId() };
    }

    public async createClaim(correlationId: string, trustAddress: string): Promise<string> {
        // create
        const create = new CashConfirmation();
        create.setCorrelationId(correlationId);

        const createTransaction = new UserTransaction();
        createTransaction.setCashConfirmation(create);
        createTransaction.setIssuer(trustAddress);

        const createReceipt = await this.asyncSubmitTx(createTransaction);
        if (createReceipt.getStatus()?.getStatusCase() !== TransactionStatus.StatusCase.FINALIZED) {
            throw new Error(`Create request correlationId=${correlationId} failed`);
        }

        return createReceipt.getId();
    }

    public async redeemRequestClaim(
        issuer: string,
        accountNumber: string,
        routingNumber: string,
        amount: number):
        Promise<{ correlationId: string, redeemRequest: string }> {

        const correlationId = this.randomCorrelationId();

        // redeem request
        const unencryptedBankAccount = new UnencryptedBankAccount();
        unencryptedBankAccount.setAccountNumber(accountNumber);
        unencryptedBankAccount.setRoutingNumber(routingNumber);

        const bankAccount = new BankAccount();
        bankAccount.setUnencryptedBankAccount(unencryptedBankAccount);

        const redeem = new RedeemRequest();
        redeem.setAmount(amount);
        redeem.setCorrelationId(correlationId);
        redeem.setBankAccount(bankAccount);

        const redeemTransaction = new UserTransaction();
        redeemTransaction.setRedeemRequest(redeem);
        redeemTransaction.setIssuer(issuer);

        const result = await this.asyncSubmitTx(redeemTransaction);
        if (result.getStatus()?.getStatusCase() !== TransactionStatus.StatusCase.FINALIZED) {
            throw new Error(`Redeem correlationId=${correlationId} failed`);
        }

        return { correlationId, redeemRequest: result.getId() };
    }

    public async redeemClaim(correlationId: string, trustAddress: string):
        Promise<string> {

        // redeem fulfillment
        const redeemFulfillment = new RedeemFulfillment();
        redeemFulfillment.setCorrelationId(correlationId);

        const redeemFulfillmentTransaction = new UserTransaction();
        redeemFulfillmentTransaction.setRedeemFulfillment(redeemFulfillment);
        redeemFulfillmentTransaction.setIssuer(trustAddress);

        const result = await this.asyncSubmitTx(redeemFulfillmentTransaction);
        if (result.getStatus()?.getStatusCase() !== TransactionStatus.StatusCase.FINALIZED) {
            throw new Error(`Redeem fulfillment correlationId=${correlationId} failed`);
        }

        return result.getId();
    }

    public async send(from: string, to: string, amount: number): Promise<TransactionUpdate> {
        const send = new Send();
        send.setTo(to);
        send.setAmount(amount);

        const sendTransaction = new UserTransaction();
        sendTransaction.setSend(send);
        sendTransaction.setIssuer(from);

        return this.asyncSubmitTx(sendTransaction);
    }

    public async getAsyncTransactionHistory(): Promise<TransactionHistory> {
      const txnHistoryRequest = new TransactionHistoryRequest();
      return new Promise((resolve, reject) => {
            this.getTransactionHistory(
                txnHistoryRequest,
                this.buildAuthMetadata(),
                (err, history) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(history);
                    }
            });
        });
    }

    public async getAsyncAddressTransactions(address: string): Promise<TransactionHistory> {
        const historyRequest = new AddressTransactionHistoryRequest();
        historyRequest.setAddress(address);
        historyRequest.setPageSize(100);
        return new Promise((resolve, reject) => {
            this.getAddressTransactions(
                historyRequest,
                this.buildAuthMetadata(),
                (err, history) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(history);
                    }
            });
        });
    }

    public async getAsyncAddressTransactionsAllAddresses(): Promise<TransactionHistory> {
        const historyRequest = new AddressTransactionHistoryRequest();
        historyRequest.setPageSize(100);
        return new Promise((resolve, reject) => {
            this.getAddressTransactions(
                historyRequest,
                this.buildAuthMetadata(),
                (err, history) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(history);
                    }
            });
        });
    }

    public async getAsyncTransactionById(txnId: string): Promise<FetchedTransaction> {
        const txnDetailsRequest = new TransactionDetailsRequest();
        txnDetailsRequest.setId(txnId);
        return new Promise((resolve, reject) => {
            this.getTransactionDetails(
                txnDetailsRequest,
                this.buildAuthMetadata(),
                (err, details) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(details);
                    }
            });
        });
    }

    public async waitOnTransactionInHistory(txnId: string, address: string, retries?: number):
        Promise<FetchedTransaction> {

        const interval = 1000;
        retries = retries || 60;

        for (let i = 0; i < retries; i++) {
            const history = await this.getAsyncAddressTransactions(address);
            const found = history.getTransactionsList()
                .find((transaction) => {
                    return transaction.getId() === txnId;
                });
            if (found) {
                return found;
            } else {
                await new Promise((resolve) => setTimeout(() => resolve(interval), interval));
            }
        }
        throw new Error("Timed out waiting on matching transaction.");
    }

    public async awaitRedeemCompletion(txnId: string, retries?: number):
        Promise<void> {

        const interval = 1000;
        retries = retries || 60;

        for (let i = 0; i < retries; i++) {
            const details = await this.getAsyncTransactionById(txnId);
            const redeem = details.getTransaction().getRedeemRequest();

            if (redeem.hasCompletingTransaction()) {
                return;
            } else {
                await new Promise((resolve) => setTimeout(() => resolve(interval), interval));
            }
        }
        throw new Error("Timed out waiting on matching transaction.");
    }

    private buildAuthMetadata(): grpc.Metadata {
        const metadata = new grpc.Metadata();
        if (this.authJwt) {
            metadata.set("authorization", `Bearer ${this.authJwt}`);
        }
        return metadata;
    }

    private randomCorrelationId(): string {
        return `0x${crypto.randomBytes(16).toString("hex")}`;
    }
}

export function parseURlForGrpcClient(url: string): string {
    const parsed = parse(url);
    return parsed.host;
}
