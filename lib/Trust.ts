import {
    Configuration,
    ServiceHealthResponse,
    ServicesApiApi,
    TotalClaimsApiApi,
    TotalClaimsResponse,
} from "xand-trust-api-client";
import { createAxiosInstance } from "./config/AxiosConfiguration";
import { XngTrustMetadata } from "./config/xng/XngTrustMetadata";

export class Trust {
    public static fromMetadata(metadata: XngTrustMetadata): Trust {
        const apiConfiguration = new Configuration();

        function removeTrailingSlash(url) {
            return url.endsWith("/") ? url.slice(0, -1) : url;
        }
        apiConfiguration.basePath = removeTrailingSlash(metadata.trustApiUrl);

        const axios = createAxiosInstance();

        const result = new Trust();
        result.servicesApi = new ServicesApiApi(apiConfiguration, undefined, axios);
        result.totalClaimsApi = new TotalClaimsApiApi(apiConfiguration, undefined, axios);

        return result;
    }

    public servicesApi: ServicesApiApi;
    public totalClaimsApi: TotalClaimsApiApi;

    public async GetHealth(): Promise<ServiceHealthResponse> {
        return (await this.servicesApi.getServiceHealth()).data;
    }

    public async GetTotalClaims(): Promise<TotalClaimsResponse> {
        return (await this.totalClaimsApi.getTotalClaims()).data;
    }
}
