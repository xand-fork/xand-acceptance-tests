import { XngMemberAccountMetadata } from "./xng/XngMemberAccountMetadata";

/// This type augments a Member's assigned account number with names
export class AccountEntry {
    public shortName: string;
    public xngAccountMetadata: XngMemberAccountMetadata;
}
