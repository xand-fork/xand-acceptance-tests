import { TreasuryPrimeAdapter } from "xand-member-api-client";
import { McbAdapterConfig } from "./McbAdapterConfig";
import { TreasuryPrimeAdapterConfig } from "./TreasuryPrimeAdapterConfig";

export default class BankAdapterConfig {
    public mcb: McbAdapterConfig;
    public treasuryPrime: TreasuryPrimeAdapterConfig;
}
