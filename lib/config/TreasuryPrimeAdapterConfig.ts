export class TreasuryPrimeAdapterConfig {
    public url: string;
    public secretApiKeyId: string;
    public secretApiKeyValue: string;
}
