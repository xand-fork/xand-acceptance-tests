
export class XngJwtInfo {
    public secret: string;
    public token: string;
}
