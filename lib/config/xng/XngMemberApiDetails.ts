import { XngJwtInfo } from "./XngJwtInfo";

export class XngMemberApiDetails {
    public memberApiUrl: string;
    public auth: XngJwtInfo | null;
}
