import { XngXandApiDetails } from "./XngXandApiDetails";

export class XngTrustMetadata {
    public address: string;
    public xandApiDetails: XngXandApiDetails;
    public trustApiUrl: string;
}
