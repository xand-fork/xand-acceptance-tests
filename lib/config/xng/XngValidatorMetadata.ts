import { XngMemberBanksMetadata } from "./XngMemberBanksMetadata";
import { XngXandApiDetails } from "./XngXandApiDetails";

export class XngValidatorMetadata {
    public address: string;
    public xandApiDetails: XngXandApiDetails;
    public banks: XngMemberBanksMetadata[];
}
