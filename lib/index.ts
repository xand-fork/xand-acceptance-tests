/* tslint:disable:no-console */
import mochaCommands = require("mocha/lib/cli/commands");
import * as path from "path";

export function setRequiredDefaultArgs(args: any) {
    // rootDir is repo root or /dist, whether referencing from source or the packaged artifacts,
    // respectively
    const libDir = __dirname;
    const rootDir = path.join(libDir, "..");

    // If passed, resolve path prefix for "ignore" argument
    if (args.ignore) {
        const modifiedPaths = args.ignore.map((relPath) =>
            path.join(rootDir, relPath),
        );
        args.ignore = modifiedPaths;
        args.exclude = modifiedPaths;
    }
    // The trailing "{js,ts}" matches typsecript test files when invoked via `ts-node`
    // and also the built *.js files when invoked in the packaged /dist folder
    const testsGlob = path.join(rootDir, "/tests/**/*.Tests.{js,ts}");
    const requiredDefaultArgs = {
        timeout: 10000,
        reporter: "mocha-multi-reporters",
        reporterOptions: { reporterEnabled: "spec,mocha-junit-reporter" },
        spec: [testsGlob],
    };

    const merged = {...args, ...requiredDefaultArgs};
    return merged;
}
export async function run(args: any) {
    // Overwrite required default args
    const overwrittenArgs = setRequiredDefaultArgs(args);
    console.log(overwrittenArgs);
    await mochaCommands.run.handler(overwrittenArgs);
}
