#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"
PROJECT_ROOT="$(realpath ${DIR}/../)"

# IN
CONFIG_TEMPLATE="${ABS_PATH_DIR}/config.template.yaml"
CONFIG_VALUES="${ABS_PATH_DIR}/.env"
VERSIONS_CONFIG="${ABS_PATH_DIR}/.versions.env"

# TEMP
TMP="${ABS_PATH_DIR}/tmp"
XNG_CONFIG="${TMP}/config.yaml"
CHAINSPEC_ZIP="${TMP}/chainspec.zip"

# OUT
OUT_DIR=${PROJECT_ROOT}
RECEIPT_DIR_NAME="generated-receipt"
OUT_NAME="generated"
OUT_PATH=${OUT_DIR}/${OUT_NAME}
RECEIPT_OUT=${OUT_PATH}/${RECEIPT_DIR_NAME}
RECEIPT_OUT_CONFIG="${RECEIPT_OUT}/generated.config.yaml"
RECEIPT_OUT_ARTIFACTS="${RECEIPT_OUT}/used-artifacts.txt"

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script generates a xand network via XNG using variables listed in .*.env files and the configuration template config.template.yaml.

    Options
      -h: Print help
      -i: Install the expected version of XNG during generation
    
    Input Variables:
      ARTIFACTORY_USER=value       Artifactory Username (required)
      ARTIFACTORY_PASS=value       Artifactory Password (required)
END
  )"
  echo -e "$HELPTEXT"
}

# Args parsing pulled from last post here: https://www.unix.com/shell-programming-and-scripting/59607-getopts-alternative.html
while true
do
  case $# in 0) break ;; esac
  case $1 in
    -i) INSTALL_XNG=1; shift ;;
    -|--) shift; break;;
    -h|--help) print_help && exit 0 ;;
    -*) echo "Invalid option $1" && print_help && exit 1 ;;
    *) break ;;
  esac
done

install_xng() {
    if [ ${INSTALL_XNG:-0} -eq 1 ]; then echo -e "Getting XNG ${XAND_NETWORK_GENERATOR_VERSION}..." &&  cargo install xand_network_generator --registry tpfs --version ${XAND_NETWORK_GENERATOR_VERSION} && echo -e "Done!"; fi
}

clean_build_dirs() {
  rm -rf ${OUT_PATH}
}

create_temp_dir() {
  mkdir -p ${TMP}
}

# Validate input .env file containing artifact versions
source_and_validate_versions_env() {
  echo "Validating ${VERSIONS_CONFIG}..."
  source $VERSIONS_CONFIG

  [[ -z "${MEMBER_API_IMAGE}" ]] && { echo "Missing MEMBER_API_IMAGE in versions env file" ; exit 1; }
  [[ -z "${VALIDATOR_IMAGE}" ]] && { echo "Missing VALIDATOR_IMAGE in versions env file" ; exit 1; }
  [[ -z "${TRUST_IMAGE}" ]] && { echo "Missing TRUST_IMAGE in versions env file" ; exit 1; }
  [[ -z "${BANK_MOCKS_IMAGE}" ]] && { echo "Missing BANK_MOCKS_IMAGE in versions env file" ; exit 1; }
  [[ -z "${XAND_NETWORK_GENERATOR_VERSION}" ]] && { echo "Missing XAND_NETWORK_GENERATOR_VERSION in versions env file" ; exit 1; }
  [[ -z "${CHAINSPEC_VERSION}" ]] && { echo "Missing CHAINSPEC_VERSION in versions env file" ; exit 1; }
  [[ -z "${TRUST_API_IMAGE}" ]] && { echo "Missing TRUST_API_IMAGE in versions env file" ; exit 1; }

  echo "${VERSIONS_CONFIG} validated."
}

get_chainspec() {
  echo -e "Getting chainspec ${CHAINSPEC_VERSION}..."

  zip_filename="chain-spec-template.${CHAINSPEC_VERSION}.zip"

  wget --directory-prefix=${TMP} --user=${ARTIFACTORY_USER}  --password=${ARTIFACTORY_PASS} -O "${CHAINSPEC_ZIP}" -q \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${zip_filename}

  echo -e "Chainspec downloaded to ${CHAINSPEC_ZIP}"
}

# Arg 1 - Target TMP Config e.g. ${XNG_CONFIG}
generate_config() {
  echo -e "Creating XNG Config for $1..."
  cp ${CONFIG_TEMPLATE} $1

  declare -A replacements

  replacements[MEMBER_API_IMAGE]=${MEMBER_API_IMAGE}
  replacements[VALIDATOR_IMAGE]=${VALIDATOR_IMAGE}
  replacements[TRUST_IMAGE]=${TRUST_IMAGE}
  replacements[TRUST_API_IMAGE]=${TRUST_API_IMAGE}
  replacements[BANK_MOCKS_IMAGE]=${BANK_MOCKS_IMAGE}

  replacements[VALIDATOR_COUNT]=${VALIDATOR_COUNT}
  replacements[MEMBER_COUNT]=${MEMBER_COUNT}
  replacements[ENABLE_AUTH]=${ENABLE_AUTH}

  for k in "${!replacements[@]}"
    do
        echo -e "\tReplacing ${k} with ${replacements[${k}]}"
        sed -i -e "s@\${${k}}@${replacements[${k}]}@" $1
    done
  echo -e "Config generated."
}

generate_network() {
  echo -e "Generating network..."
  xand_network_generator generate  \
    --config ${XNG_CONFIG} \
    --chainspec-zip ${CHAINSPEC_ZIP} \
    --output-dir ${OUT_PATH}

  mkdir -p ${RECEIPT_OUT}
  cp ${XNG_CONFIG} ${RECEIPT_OUT_CONFIG}
  echo "XNG Version: ${XAND_NETWORK_GENERATOR_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
  echo "Chainspec Version: ${CHAINSPEC_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
  echo -e "network generated at ${OUT_PATH}."
}

delete_temp_dir() {
    rm -rf ${TMP}
}

source_and_validate_versions_env
delete_temp_dir
clean_build_dirs
create_temp_dir
install_xng
get_chainspec

source ${CONFIG_VALUES}
generate_config ${XNG_CONFIG}

generate_network
delete_temp_dir
