// import "mocha";
import { expect } from "chai";
import {
  AccountsApi,
  McbAdapter,
  TreasuryPrimeAdapter,
  UpdateBank,
} from "xand-member-api-client";
import { sortAccountsByShortName } from "../../lib/BankAccount";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a banks api", () => {
  let awesomecoAccounts: AccountsApi;
  let coolIncAccounts: AccountsApi;
  let accountId: number;
  let awesomeCo: Member;
  let coolInc: Member;

  before(async () => {
    const testContext = new TestContext();
    awesomeCo = await testContext.awesomeCo;
    awesomecoAccounts = awesomeCo.accountsApi;
    coolInc = await testContext.coolInc;
    coolIncAccounts = coolInc.accountsApi;
  });

  describe("And I want to add banks", () => {
    before(async () => {
      await awesomeCo.DeleteAllBanks();
    });

    it("Then I can add an mcb bank", async () => {
      const adapter: McbAdapter = {
        url: "http://localhost:8877/metropolitan",
        secretUserName: "mcb-secret-user-name",
        secretClientAppIdent: "mcb-secret-client-app-ident",
        secretOrganizationId: "mcb-secret-organization-id",
        secretPassword: "mcb-secret-password",
      };
      const bank: UpdateBank = {
        name: "mcb",
        routingNumber: "121141343",
        claimsAccount: "5555555555",
        adapter: {
          mcb: adapter,
        },
      };

      const response = (await awesomeCo.banksApi.postBanks(bank)).data;
      expect(response.id).to.be.greaterThan(0);
    });

    it("Then I can add a provident bank", async () => {
      const adapter: TreasuryPrimeAdapter = {
        url: "http://example.com",
        secretApiKeyId: "provident-secret-api-key-id",
        secretApiKeyValue: "provident-secret-api-key-value",
      };
      const bank: UpdateBank = {
        name: "provident-bank",
        routingNumber: "743214",
        claimsAccount: "54321",
        adapter: {
          treasuryPrime: adapter,
        },
      };

      const response = (await awesomeCo.banksApi.postBanks(bank)).data;
      expect(response.id).to.be.greaterThan(0);
    });
  });

  describe("And a set of bank accounts have been registered for awesomeco", () => {
    before(async () => {
      await (await awesomeCo.DeleteAllBanks()).EnsureBanksAreConfigured();

      const account = awesomeCo.selectAccountByName("awesomeco_mcb_0");
      accountId = account.id;
    });

    it("Then I can get all banks", async () => {
      const data = (await awesomecoAccounts.getAccounts()).data;

      const byShortName = sortAccountsByShortName(data);

      const bank = byShortName[0];

      expect(bank.shortName).to.equal("awesomeco_mcb_0");
    });

    it("Then I can Get a bank By Id", async () => {
      const data = (await awesomecoAccounts.getAccount(accountId)).data;
      expect(data.shortName).to.equal("awesomeco_mcb_0");
      expect(data.routingNumber).to.equal("121141343");
    });

    it("Then I can Get the Balance for a bank", async () => {
      const data = (await awesomecoAccounts.getBankBalance(accountId)).data;
      expect(data.currentBalanceInMinorUnit).to.be.greaterThan(0);
      expect(data.availableBalanceInMinorUnit).to.be.greaterThan(0);
    });
  });

  describe("And a set of bank accounts have been registered for coolinc", () => {
    before(async () => {
      await (await coolInc.DeleteAllBanks()).EnsureBanksAreConfigured();

      const account = coolInc.selectAccountByName("coolinc_mcb_0");
      accountId = account.id;
    });

    it("Then I can get all banks", async () => {
      const data = (await coolIncAccounts.getAccounts()).data;

      const byShortName = sortAccountsByShortName(data);

      const bank = byShortName[0];

      expect(bank.shortName).to.equal("coolinc_mcb_0");
    });

    it("Then I can Get a bank By Id", async () => {
      const data = (await coolIncAccounts.getAccount(accountId)).data;
      expect(data.shortName).to.equal("coolinc_mcb_0");
      expect(data.routingNumber).to.equal("121141343");
    });

    it("Then I can Get the Balance for a bank", async () => {
      const data = (await coolIncAccounts.getBankBalance(accountId)).data;
      expect(data.currentBalanceInMinorUnit).to.be.greaterThan(0);
      expect(data.availableBalanceInMinorUnit).to.be.greaterThan(0);
    });
  });
});
