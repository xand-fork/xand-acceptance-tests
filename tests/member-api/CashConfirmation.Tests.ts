import { expect } from "chai";
import {
    BankAccountBalance,
    FundClaimsReceipt,
    OperationType,
    Receipt,
    Transaction,
    TransactionState,
    XandBalance,
} from "xand-member-api-client";
import { EnhancedXandApi } from "../../lib/EnhancedXandApi";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a Member named Awesomeco who wants to Create XAND", () => {
    const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;
    const TRANSACTION_ID_FORMAT = "^0x([A-Fa-f]|\\d){64}$";

    let context: TestContext;
    let awesomeCo: Member;
    let coolInc: Member;
    let awesomeCoXandApiClient: EnhancedXandApi;
    let coolIncXandApiClient: EnhancedXandApi;
    let trustXandApiClient: EnhancedXandApi;

    before(async () => {
        context = new TestContext();

        awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
        awesomeCo.selectAccountByName("awesomeco_mcb_0");

        coolInc = await context.coolInc.EnsureBanksAreConfigured();
        coolInc.selectAccountByName("coolinc_mcb_0");

        awesomeCoXandApiClient = context.getAwesomeCoXandApi();
        coolIncXandApiClient = context.getCoolIncXandApi();
        trustXandApiClient = context.getTrustXandApi();
    });

    describe("When I submit a create request", () => {
        let receipt: Receipt;
        let startingBalance: BankAccountBalance;
        let submittedAwesomeCoCreateRequest: Transaction;
        const amount = 100;

        // Passing arrow functions (“lambdas”) to Mocha is discouraged.
        // Due to the lexical binding of this, such functions are unable
        // to access the Mocha context.
        //
        // Translation: In order to call `this.timeout()` we need to use the
        // `async function` syntax instead of `async () =>` syntax.
        //
        // tslint:disable-next-line: only-arrow-functions
        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            receipt = await awesomeCo.SubmitCreateRequest(amount);
            expect(receipt).to.exist;

            submittedAwesomeCoCreateRequest = await awesomeCo.GetTransactionById(
                receipt.transactionId,
            );
            expect(submittedAwesomeCoCreateRequest).to.exist;

            startingBalance = await awesomeCo.GetBankAccountBalance();
            expect(startingBalance).to.exist;
        });

        describe("When I transfer funds to the reserve", () => {
            let xandBalance: XandBalance;
            let transfer: FundClaimsReceipt;

            before(async () => {
                xandBalance = await awesomeCo.GetXandBalance();
                transfer = await awesomeCo.SubmitTransferToReserve(
                    submittedAwesomeCoCreateRequest.amountInMinorUnit,
                    receipt,
                );
                expect(transfer).to.exist;
            });

            describe("And the Create is fulfilled by the trustee", async () => {
                let fulfillment: Transaction;

                // Passing arrow functions (“lambdas”) to Mocha is discouraged.
                // Due to the lexical binding of this, such functions are unable
                // to access the Mocha context.
                //
                // Translation: In order to call `this.timeout()` we need to use the
                // `async function` syntax instead of `async () =>` syntax.
                //
                before(async function() {
                    this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                    fulfillment = await awesomeCo.awaitConfirmation(
                        submittedAwesomeCoCreateRequest.transactionId,
                    );
                });

                it(
                    "Then I can determine from my transaction history that I submitted the request",
                    async () => {
                        const awesomeCoHistory = await awesomeCo.GetHistory();
                        const createReqTx = awesomeCoHistory.transactions.find((transaction) =>
                            transaction.transactionId ===
                                submittedAwesomeCoCreateRequest.transactionId,
                        );
                        expect(createReqTx.signerAddress).to.equal(awesomeCo.address);
                    },
                );

                it("Then other members can't determine I submitted the request", async () => {
                    const coolIncHistory =
                        await coolIncXandApiClient.getAsyncTransactionHistory();
                    const createReqTx =
                        coolIncHistory.getTransactionsList().find((transaction) =>
                            transaction.getId() ===
                                submittedAwesomeCoCreateRequest.transactionId,
                        );

                    // Xand API's get history endpoint only returns transactions that have
                    // required fields populated. Any transactions not returned aren't
                    // relevant to that caller. As long as that remains true `createReqTx`
                    // will be `undefined`.
                    //
                    // This test confirms coolInc cannot determine from the API that awesomeCo
                    // sent the create request, but it's technically not enough to confirm the
                    // information isn't available in the unfiltered, raw blockchain data.

                    if (createReqTx !== undefined) {
                        expect(createReqTx.getTransaction().getIssuer())
                            .to
                            .not
                            .equal(awesomeCo.address);
                    }
                });

                it("Then the trust can determine I submitted it", async () => {
                    const trustHistory = await trustXandApiClient.getAsyncTransactionHistory();
                    const createReqTx = trustHistory.getTransactionsList().find((transaction) =>
                        transaction.getId() === submittedAwesomeCoCreateRequest.transactionId,
                    );

                    expect(createReqTx.getTransaction().getIssuer()).to.equal(awesomeCo.address);
                });

                it("Then the fulfillment is correctly recorded", () => {
                    expect(fulfillment).to.exist;
                    expect(fulfillment.amountInMinorUnit).to.equal(0);
                    expect(fulfillment.operation).to.equal(OperationType.Create);
                    expect(fulfillment.signerAddress).to.equal(
                        context.GetTrusteeAddress(),
                    );
                    expect(fulfillment.transactionId).to.exist;
                });

                it("Then the xand balance is increased by the amount of the Transfer", async () => {
                    const newBalance = (await awesomeCo.GetXandBalance())
                        .balanceInMinorUnit;
                    const expectedBalance =
                        xandBalance.balanceInMinorUnit +
                        submittedAwesomeCoCreateRequest
                            .amountInMinorUnit;
                    expect(newBalance).to.equal(expectedBalance);
                });

                it("Then the confirmation id on the create request is populated.", async () => {
                    submittedAwesomeCoCreateRequest = await awesomeCo.GetTransactionById(
                        receipt.transactionId,
                    );
                    const fulfilled = await awesomeCo.GetTransactionById(
                        fulfillment.transactionId,
                    );

                    expect(submittedAwesomeCoCreateRequest.confirmationId).to.equal(
                        fulfilled.transactionId,
                    );
                });

                it("Then create request state is updated to confirmed.", async () => {
                    submittedAwesomeCoCreateRequest = await awesomeCo.GetTransactionById(
                        receipt.transactionId,
                    );
                    expect(submittedAwesomeCoCreateRequest.status.state).to.equal(
                        TransactionState.Confirmed,
                    );
                });

                it(
                    "Then I can retrieve the transaction details for the confirmation.",
                    async () => {
                    const create = await awesomeCo.GetTransactionById(
                        fulfillment.transactionId,
                    );
                    expect(create).to.exist;
                    expect(create.transactionId).to.equal(fulfillment.transactionId);
                    expect(create.operation).to.equal(OperationType.Create);
                });

                it(
                    "Then I can directly query Xand API to see Create-related transactions",
                    async () => {
                        const relevantTxns: Transaction[] =
                            [submittedAwesomeCoCreateRequest, fulfillment];

                        const xandApiHistory = await awesomeCoXandApiClient
                            .getAsyncAddressTransactions(
                            awesomeCo.address,
                        );

                        const historyIds = xandApiHistory
                            .getTransactionsList().map((e) => e.getId());
                        for (const txn of relevantTxns) {
                            expect(historyIds).contains(
                                txn.transactionId,
                                `Did not find ${txn.transactionId} of ${txn} in Xand API history.`,
                            );
                        }
                    });
            });
        });
    });
});
