import { expect } from "chai";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

// Passing arrow functions (“lambdas”) to Mocha is discouraged.
// Due to the lexical binding of this, such functions are unable
// to access the Mocha context.
//
// Translation: In order to call `this.timeout()` we need to use the
// `async function` syntax instead of `async () =>` syntax.
//
// tslint:disable: only-arrow-functions

describe("Given a Member named Awesomeco Wants to view transaction history", () => {
  const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;

  let context: TestContext;
  let awesomeCo: Member;

  before(async () => {
    context = new TestContext();
    awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
    awesomeCo.selectAccountByName("awesomeco_mcb_0");
  });

  describe("And I fully complete three creates", function() {
    const receiptIds: string[] = [];
    const amounts = [100, 1000, 10];

    before(async function() {
      this.timeout(
        MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED * amounts.length,
      );
      for (const amnt of amounts) {
        const receipt = await awesomeCo.SubmitCreateRequest(amnt);
        await awesomeCo.SubmitTransferToReserve(amnt, receipt);
        receiptIds.push(receipt.transactionId);
        await awesomeCo.awaitConfirmation(receipt.transactionId);
      }
    });

    it("Then the transaction history shows the create requests", async function() {
      const transactions = await awesomeCo.GetHistory();
      const txIds = transactions.transactions.map((t) => t.transactionId);
      for (const receiptId of receiptIds) {
        expect(txIds.includes(receiptId));
      }
    });

    it("Then a paginated history request can return only the latest item", async function() {
      const transactions = await awesomeCo.GetHistory(0, 1);
      expect(transactions.transactions.length).to.equal(1);
      expect(transactions.transactions[0].transactionId).to.equal(
        receiptIds[receiptIds.length - 1],
      );
    });
  });
});
