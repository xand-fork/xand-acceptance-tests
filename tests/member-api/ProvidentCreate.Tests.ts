import { expect } from "chai";

import {
    AccountsApi,
    BankAccount,
    BankAccountBalance,
    FundClaimsReceipt,
    Receipt,
    Transaction,
    TransactionState,
    XandBalance,
} from "xand-member-api-client";
import TestContext from "../../lib/TestContext";

describe("Given a provident bank account", () => {

    let context: TestContext;
    let providentAccount: BankAccount;
    const providentBankAccountName = "awesomeco_provident_0";

    before(async () => {
        context = new TestContext();
        await context.awesomeCo.EnsureBanksAreConfigured();
        providentAccount = context.awesomeCo.selectAccountByName(providentBankAccountName);
    });

    describe("Given a banks api", () => {

        let accountsApi: AccountsApi;

        before(() => {
            accountsApi = context.awesomeCo.accountsApi;
        });

        // tslint:disable-next-line:only-arrow-functions
        describe("When I get balances", function() {
            it("Then current and available balances are greater than zero", async () => {
                const data = (await accountsApi.getBankBalance(providentAccount.id)).data;
                expect(data.currentBalanceInMinorUnit).to.be.greaterThan(0);
                expect(data.availableBalanceInMinorUnit).to.be.greaterThan(0);
            });
        });
    });

    describe("Given a wallet", () => {
        const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;

        describe("And I have submitted a create request", () => {
            let receipt: Receipt;
            let startingBalance: BankAccountBalance;
            let createRequest: Transaction;

            // tslint:disable-next-line: only-arrow-functions
            before(async function() {
                this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

                receipt = await context.awesomeCo.SubmitCreateRequest(100);
                expect(receipt).to.exist;

                createRequest = await context.awesomeCo.GetTransactionById(receipt.transactionId);
                expect(createRequest).to.exist;

                startingBalance = await context.awesomeCo.GetBankAccountBalance();
                expect(startingBalance).to.exist;
            });

            describe("When I transfer funds to the reserve", () => {

                let xandBalance: XandBalance;
                let transfer: FundClaimsReceipt;

                before(async () => {
                    xandBalance = await context.awesomeCo.GetXandBalance();
                    transfer = await context.awesomeCo
                        .SubmitTransferToReserve(createRequest.amountInMinorUnit, receipt);
                    expect(transfer).to.exist;
                });

                it("Then the bank balance is reduced by the amount of the transfer.", async () => {
                    const newBalance = await context.awesomeCo.GetBankAccountBalance();
                    const expectedBalance = startingBalance.currentBalanceInMinorUnit
                        - createRequest.amountInMinorUnit;

                    expect(newBalance.currentBalanceInMinorUnit).to.equal(expectedBalance);
                    expect(newBalance.availableBalanceInMinorUnit).to.equal(expectedBalance);
                });

                describe("And the Create is fulfilled by the trustee", () => {

                    let fulfillment: Transaction;

                    before(async function() {
                        this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                        fulfillment = await context.awesomeCo
                            .awaitConfirmation(createRequest.transactionId);
                    });

                    it("Then transaction status is committed", () => {
                        expect(fulfillment.status.state).to.equal(TransactionState.Confirmed);
                    });
                });
            });
        });
    });
});
