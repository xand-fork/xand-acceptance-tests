import { expect } from "chai";

import {
    AccountsApi,
    BankAccount,
    BankAccountBalance,
    FundClaimsReceipt,
    Receipt,
    Transaction,
    TransactionState,
    XandBalance,
} from "xand-member-api-client";
import TestContext from "../../lib/TestContext";

describe("Given a provident bank account", () => {

    let context: TestContext;
    let providentAccount: BankAccount;
    const providentBankAccountName = "awesomeco_provident_0";

    before(async () => {
        context = new TestContext();
        await context.awesomeCo.EnsureBanksAreConfigured();
        providentAccount = context.awesomeCo.selectAccountByName(providentBankAccountName);
    });

    describe("Given a banks api", () => {

        let accountsApi: AccountsApi;

        before(() => {
            accountsApi = context.awesomeCo.accountsApi;
        });
    });

    describe("Given a wallet", () => {
        const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;

        describe("And I have submitted a create request", () => {
            let receipt: Receipt;
            let startingBalance: BankAccountBalance;
            let createRequest: Transaction;

            // tslint:disable-next-line: only-arrow-functions
            before(async function() {
                this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

                receipt = await context.awesomeCo.SubmitCreateRequest(100);
                expect(receipt).to.exist;

                createRequest = await context.awesomeCo.GetTransactionById(receipt.transactionId);
                expect(createRequest).to.exist;

                startingBalance = await context.awesomeCo.GetBankAccountBalance();
                expect(startingBalance).to.exist;
            });

            describe("When I transfer funds to the reserve", () => {

                let xandBalance: XandBalance;
                let transfer: FundClaimsReceipt;

                before(async () => {
                    xandBalance = await context.awesomeCo.GetXandBalance();
                    transfer = await context.awesomeCo
                        .SubmitTransferToReserve(createRequest.amountInMinorUnit, receipt);
                    expect(transfer).to.exist;
                });

                describe("And the Create is fulfilled by the trustee", () => {

                    let fulfillment: Transaction;

                    before(async function() {
                        this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                        fulfillment = await context.awesomeCo
                            .awaitConfirmation(createRequest.transactionId);
                    });
                });
            });

            describe("And I have at least $100 on the network", () => {
                const MAX_TIME_TO_WAIT_ON_FULFILLMENT = 120000;
                const TRANSACTION_ID_FORMAT = "^0x([A-Fa-f]|\\d){64}$";
                // tslint:disable-next-line: only-arrow-functions
                before(async function() {
                    this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

                    await context.awesomeCo.SetWalletBalanceToAtLeast(100);
                });

                describe("Then I can redeem back to Provident", async () => {

                    let redeemFulfillment: Transaction;
                    let startingBankBalance: BankAccountBalance;
                    let xandBalance: XandBalance;
                    const amountToRedeem = 50;

                    before(async function() {
                        this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

                        startingBankBalance = await context.awesomeCo.GetBankAccountBalance();
                        xandBalance = await context.awesomeCo.GetXandBalance();

                        receipt = await context.awesomeCo.SubmitRedeemRequest(amountToRedeem);
                        redeemFulfillment = await context.awesomeCo
                            .awaitConfirmation(receipt.transactionId);
                    });

                    it("Then the transaction is committed.", () => {
                        expect(redeemFulfillment.status.state).to.equal(TransactionState.Confirmed);
                    });

                    it(
                        "And the bank balance is increased by the amount of the redeem.",
                        async () => {
                            const newBalance = await context.awesomeCo.GetBankAccountBalance();
                            const expectedBalance = startingBankBalance.currentBalanceInMinorUnit
                                + amountToRedeem;

                            expect(newBalance
                                .currentBalanceInMinorUnit)
                                .to.equal(expectedBalance, "currentBalanceInMinorUnit");
                            expect(newBalance
                                .availableBalanceInMinorUnit)
                                .to.equal(expectedBalance, "availableBalanceInMinorUnit");
                        });
                });
            });
        });
    });
});
