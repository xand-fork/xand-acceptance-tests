import { expect } from "chai";
import {
  BankAccount,
  BankAccountBalance,
  OperationType,
  Receipt,
  Transaction,
  XandBalance,
} from "xand-member-api-client";
import { EnhancedXandApi } from "../../lib/EnhancedXandApi";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a Member named Awesomeco wants to Redeem XAND", () => {
  const MAX_TIME_TO_WAIT_ON_FULFILLMENT = 120000;
  const TRANSACTION_ID_FORMAT = "^0x([A-Fa-f]|\\d){64}$";

  let context: TestContext;
  let awesomeCo: Member;
  let awesomeCoXandApiClient: EnhancedXandApi;

  before(async () => {
    context = new TestContext();
    awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
    awesomeCo.selectAccountByName("awesomeco_mcb_0");
    awesomeCoXandApiClient = context.getAwesomeCoXandApi();
  });

  describe("And I have at least $1 on the network", () => {
    // Passing arrow functions (“lambdas”) to Mocha is discouraged.
    // Due to the lexical binding of this, such functions are unable
    // to access the Mocha context.
    //
    // Translation: In order to call `this.timeout()` we need to use the
    // `async function` syntax instead of `async () =>` syntax.
    //
    // tslint:disable-next-line: only-arrow-functions
    before(async function() {
      this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

      await awesomeCo.SetWalletBalanceToAtLeast(100);
    });

    describe("Then I can redeem back to the same bank", async () => {
      let redeemFulfillment: Transaction;
      let redeemTx: Transaction;
      let startingBankBalance: BankAccountBalance;
      let xandBalance: XandBalance;
      let receipt: Receipt;
      const amountToRedeem = 50;

      // Passing arrow functions (“lambdas”) to Mocha is discouraged.
      // Due to the lexical binding of this, such functions are unable
      // to access the Mocha context.
      //
      // Translation: In order to call `this.timeout()` we need to use the
      // `async function` syntax instead of `async () =>` syntax.
      //

      before(async function() {
        this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

        startingBankBalance = await awesomeCo.GetBankAccountBalance();
        xandBalance = await awesomeCo.GetXandBalance();

        receipt = await awesomeCo.SubmitRedeemRequest(amountToRedeem);
        redeemTx = await awesomeCo.awaitResolution(receipt.transactionId);
        redeemFulfillment = await awesomeCo.awaitResolution(
          redeemTx.confirmationId,
        );
      });

      it("Then the transaction id is in the correct format.", () => {
        expect(redeemFulfillment.transactionId).to.matches(
          new RegExp(TRANSACTION_ID_FORMAT),
        );
      });

      it("And the fulfillment is correctly populated.", () => {
        expect(redeemFulfillment).to.exist;
        expect(redeemFulfillment.amountInMinorUnit).to.equal(0);
        expect(redeemFulfillment.operation).to.equal(
          OperationType.RedeemFulfillment,
        );
        expect(redeemFulfillment.transactionId).to.exist;
        expect(redeemFulfillment.signerAddress).to.equal(
          context.GetTrusteeAddress(),
        );
      });

      it("And the bank balance is increased by the amount of the redeem.", async () => {
        const newBalance = await awesomeCo.GetBankAccountBalance();
        const expectedBalance =
          startingBankBalance.currentBalanceInMinorUnit + amountToRedeem;

        expect(newBalance.currentBalanceInMinorUnit).to.equal(
          expectedBalance,
          "currentBalanceInMinorUnit",
        );
        expect(newBalance.availableBalanceInMinorUnit).to.equal(
          expectedBalance,
          "availableBalanceInMinorUnit",
        );
      });

      it("And the xand balance is decreased by the amount of the Transfer", async () => {
        const newBalance = (await awesomeCo.GetXandBalance())
          .balanceInMinorUnit;
        const expectedBalance = xandBalance.balanceInMinorUnit - amountToRedeem;
        expect(newBalance).to.equal(expectedBalance);
      });
    });

    describe("Then I can redeem back to a different bank", () => {
      let redeemFulfillment: Transaction;
      let startingBankBalance: BankAccountBalance;
      let xandBalance: XandBalance;
      let redeem: Transaction;
      let otherBank: BankAccount;
      before(() => {
        otherBank = context.awesomeCo.selectAccountByName(
          "awesomeco_provident_0",
        );
      });

      // Passing arrow functions (“lambdas”) to Mocha is discouraged.
      // Due to the lexical binding of this, such functions are unable
      // to access the Mocha context.
      //
      // Translation: In order to call `this.timeout()` we need to use the
      // `async function` syntax instead of `async () =>` syntax.
      //
      before(async function() {
        this.timeout(MAX_TIME_TO_WAIT_ON_FULFILLMENT);

        startingBankBalance = await awesomeCo.GetBankAccountBalance(
          otherBank.id,
        );
        xandBalance = await awesomeCo.GetXandBalance();

        const receipt = await awesomeCo.SubmitRedeemRequest(
          50,
          otherBank.id,
        );
        redeem = await awesomeCo.GetTransactionById(receipt.transactionId);

        redeemFulfillment = await awesomeCo.awaitConfirmation(
          receipt.transactionId,
        );
      });

      it("And the fulfillment is correctly populated.", () => {
        expect(redeemFulfillment).to.exist;
        expect(redeemFulfillment.amountInMinorUnit).to.equal(0);
        expect(redeemFulfillment.operation).to.equal(
          OperationType.RedeemFulfillment,
        );
        expect(redeemFulfillment.transactionId).to.exist;
        expect(redeemFulfillment.signerAddress).to.equal(
          context.GetTrusteeAddress(),
        );
      });

      it("And the bank balance is increased by the amount of the redeem.", async () => {
        const newBalance = await awesomeCo.GetBankAccountBalance(otherBank.id);
        const expectedBalance =
          startingBankBalance.currentBalanceInMinorUnit +
          redeem.amountInMinorUnit;

        expect(newBalance.currentBalanceInMinorUnit).to.equal(expectedBalance);
        expect(newBalance.availableBalanceInMinorUnit).to.equal(
          expectedBalance,
        );
      });

      it("And the xand balance is decreased by the amount of the transfer", async () => {
        const newBalance = (await awesomeCo.GetXandBalance())
          .balanceInMinorUnit;
        const expectedBalance =
          xandBalance.balanceInMinorUnit - redeem.amountInMinorUnit;
        expect(newBalance).to.equal(expectedBalance);
      });

      it("Then I can directly query Xand API to see Redeem-related transactions",
          async () => {
            const relevantTxns: Transaction[] = [redeem, redeemFulfillment];

            const xandApiHistory = await awesomeCoXandApiClient.getAsyncAddressTransactions(
                awesomeCo.address,
            );

            const historyIds = xandApiHistory.getTransactionsList().map((e) => e.getId());
            for (const txn of relevantTxns) {
              expect(historyIds).contains(
                  txn.transactionId,
                  `Did not find ${txn.transactionId} of ${txn} in Xand API history.`,
              );
            }
          });
    });
  });
});
