import { expect } from "chai";
import {
    BankAccountBalance,
    FundClaimsReceipt,
    Receipt,
    Transaction,
    TransactionState,
} from "xand-member-api-client";
import Member from "../../lib/Member";
import TestContext from "../../lib/TestContext";

describe("Given a Trust API in a running XAND network", () => {
    const MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED = 120000;
    let context: TestContext;
    let awesomeCo: Member;
    let coolInc: Member;

    before(async () => {
        context = new TestContext();
        awesomeCo = await context.awesomeCo.EnsureBanksAreConfigured();
        coolInc = await context.coolInc.EnsureBanksAreConfigured();
    });
    describe("When I probe the total claims endpoint with no claims on the network", () => {
        let totalClaimsResponse;
        before(async () => {
            totalClaimsResponse = await context.trust.GetTotalClaims();
        });

        it("Then the total claims endpoint reports no claims on the network", () => {
            expect(totalClaimsResponse.total_claims_in_minor_units === 0);
        });

    });
    describe("When Member 1 submits a create request", () => {
        const createAmount = 100;
        let createReceipt: Receipt;
        let startingBalance: BankAccountBalance;
        let submittedAwesomeCoCreateRequest: Transaction;
        before(async function() {
            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

            createReceipt = await awesomeCo.SubmitCreateRequest(createAmount);
            expect(createReceipt).to.exist;

            submittedAwesomeCoCreateRequest = await awesomeCo.GetTransactionById(
                createReceipt.transactionId,
            );
            expect(submittedAwesomeCoCreateRequest).to.exist;

            startingBalance = await awesomeCo.GetBankAccountBalance();
            expect(startingBalance).to.exist;
        });

        describe("And transfers funds to the reserve", () => {
            let transfer: FundClaimsReceipt;
            before(async () => {
                transfer = await awesomeCo.SubmitTransferToReserve(
                    submittedAwesomeCoCreateRequest.amountInMinorUnit,
                    createReceipt,
                );
                expect(transfer).to.exist;
            });

            describe("And the trust fulfills the create request", () => {
                let fulfillment: Transaction;
                before(async function() {
                    this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                    fulfillment = await awesomeCo.awaitConfirmation(
                        submittedAwesomeCoCreateRequest.transactionId,
                    );
                });
                describe("When I probe the total claims endpoint", () => {
                    let totalClaimsResponse;
                    before(async () => {
                        totalClaimsResponse = await context.trust.GetTotalClaims();
                    });
                    it("Then the total claims endpoint reports the expected claims on the network",
                        () => {
                        expect(totalClaimsResponse.total_claims_in_minor_units === createAmount);
                    });
                });
                describe("When Member 1 sends claims to Member 2", () => {
                    const sendAmount = 30;
                    let send: Transaction;
                    let recipientAddress: string;
                    let senderStartingBalance: number;
                    let receiverStartingBalance: number;
                    let sendReceipt: Receipt;
                    before(async function() {
                        this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                        recipientAddress = coolInc.address;
                        senderStartingBalance =
                            (await awesomeCo.GetXandBalance()).balanceInMinorUnit;
                        receiverStartingBalance =
                            (await coolInc.GetXandBalance()).balanceInMinorUnit;

                        sendReceipt = await awesomeCo.SubmitSend(recipientAddress, sendAmount);

                        send = await awesomeCo.GetTransactionById(sendReceipt.transactionId);
                    });
                    describe("And the send transaction is completed", () => {
                        before(async function() {
                            this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);
                            expect(send).to.exist;
                            expect(send.status.state).to.equal(TransactionState.Confirmed);
                            await coolInc.awaitResolution(sendReceipt.transactionId);

                            const receiverEndingBalance =
                                (await coolInc.GetXandBalance()).balanceInMinorUnit;
                            expect(receiverEndingBalance - receiverStartingBalance)
                                .to.equal(sendAmount);
                            const senderEndingBalance =
                                (await awesomeCo.GetXandBalance()).balanceInMinorUnit;
                            expect(senderEndingBalance - senderStartingBalance)
                                .to.equal(-sendAmount);
                        });
                        describe("When I probe the total claims endpoint", () => {
                            let totalClaimsResponse;
                            before(async () => {
                                totalClaimsResponse = await context.trust.GetTotalClaims();
                            });
                            it("Then the total claims endpoint reports no change in claims on the network",
                                () => {
                                    expect(totalClaimsResponse.total_claims_in_minor_units
                                        === createAmount);
                                });
                        });
                    });
                });
                describe("When Member 1 redeems their claims", () => {
                    let redeemFulfillment: Transaction;
                    let redeemTx: Transaction;
                    let receipt: Receipt;
                    let redeemAmount: number;
                    before(async function() {
                       this.timeout(MAX_TIME_TO_WAIT_ON_TRANSACTION_TO_BE_COMMITTED);

                       redeemAmount = (await awesomeCo.GetXandBalance()).balanceInMinorUnit;

                       receipt = await awesomeCo.SubmitRedeemRequest(redeemAmount);
                       redeemTx = await awesomeCo.awaitResolution(receipt.transactionId);
                       redeemFulfillment = await awesomeCo.awaitResolution(
                           redeemTx.confirmationId,
                       );
                    });
                    describe("When I probe the total claims endpoint", () => {
                        let totalClaimsResponse;
                        before(async () => {
                            totalClaimsResponse = await context.trust.GetTotalClaims();
                        });
                        it("Then the total claims endpoint reports the create amount minus the redeem amount in claims on the network", () => {
                            expect(totalClaimsResponse.total_claims_in_minor_units
                                === createAmount - redeemAmount);
                        });
                    });
                });
            });
        });
    });
});
