import { expect } from "chai";
import { EnhancedXandApi } from "../../lib/EnhancedXandApi";
import TestContext from "../../lib/TestContext";

describe("Given an instance of XAND API", () => {
  let context: TestContext;
  let awesomeCoXandApiClient: EnhancedXandApi;
  let awesomeCo: string;

  before(() => {
    context = new TestContext();
    awesomeCo = context.awesomeCo.config.xngMemberMetadata.address;
  });

  describe("and a gRPC client", () => {
    before(() => {
      awesomeCoXandApiClient = context.getAwesomeCoXandApi();
    });

    // Intent of this test is to confirm the gRPC server is correctly configured.
    // This is particularly useful to test against a Xand API deployed to a K8s cluster, where
    // nginx and ingress settings need to be correctly set to allow gRPC requests.
    it("I can hit the gRPC server and make a trivial request against the Xand API", async () => {
      const blockstamp = await awesomeCoXandApiClient.getBlock();
      expect(blockstamp.getBlockNumber()).to.be.at.least(0);
    });
  });
});
